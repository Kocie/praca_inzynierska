package com.example.komputer.mycook;

import android.location.Location;

interface OnLocationChanged {
    void onLocationChanged(Location location);
}
