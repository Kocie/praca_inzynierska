package com.example.komputer.mycook;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class VideoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.recyclerViewFeed)
    RecyclerView recyclerViewFeed;


    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;

    YTRecyclerAdapter mRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        ButterKnife.bind(this);
        // prepare data for list
        List<YoutubeVideo> youtubeVideos = prepareList();
        mRecyclerAdapter = new YTRecyclerAdapter(youtubeVideos);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewFeed.setLayoutManager(mLayoutManager);
        recyclerViewFeed.setItemAnimator(new DefaultItemAnimator());
        recyclerViewFeed.setAdapter(mRecyclerAdapter);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();
    /*    navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.nav_home:
                        Intent anIntent = new Intent(getApplicationContext(), Home.class);
                        startActivity(anIntent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_search:
                        Intent aIntent = new Intent(getApplicationContext(), Search.class);
                        startActivity(aIntent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_map:
                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(intent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_video:
                        Intent vintent = new Intent(getApplicationContext(), VideoActivity.class);
                        startActivity(vintent);
                        drawer.closeDrawers();
                        break;

                }
                return false;
            }
        });*/
    }

    private List<YoutubeVideo> prepareList() {
        ArrayList mYoutubeVideo = new ArrayList();
        // add first item
        YoutubeVideo video1 = new YoutubeVideo();
        video1.setId(1l);
        video1.setImageUrl("https://cdn.pixabay.com/photo/2016/07/22/05/09/delicious-1534216_960_720.jpg");
        video1.setTitle("TASTY CURRY CHICKEN | Easy food recipes for dinner to make at allbars");
        video1.setVideoId("gXpiGKf7ixA");
        mYoutubeVideo.add(video1);

        // add second item
        YoutubeVideo video2 = new YoutubeVideo();
        video2.setId(2l);
        video2.setImageUrl("https://cdn.pixabay.com/photo/2018/08/29/19/01/fig-3640553_960_720.jpg");
        video2.setTitle("9 Snacks To Make For Your Next Party");
        video2.setVideoId("wwUSi-GVqsY");
        mYoutubeVideo.add(video2);

        // add third item
        YoutubeVideo video3 = new YoutubeVideo();
        video3.setId(3l);
        video3.setImageUrl("https://cdn.pixabay.com/photo/2017/09/18/14/49/egg-sandwich-2761894_960_720.jpg");
        video3.setTitle("Top 5 Tasty Breakfast Recipes");
        video3.setVideoId("UJfqp1dmJ3I");
        mYoutubeVideo.add(video3);

        // add four item
        YoutubeVideo video4 = new YoutubeVideo();
        video4.setId(4l);
        video4.setImageUrl("https://cdn.pixabay.com/photo/2016/08/16/21/35/grill-1599038_960_720.jpg");
        video4.setTitle("The 5 Best Fried Chicken Recipes");
        video4.setVideoId("mfIgYdJCwUA");
        mYoutubeVideo.add(video4);

        // add four item
        YoutubeVideo video5 = new YoutubeVideo();
        video5.setId(5l);
        video5.setImageUrl("https://cdn.pixabay.com/photo/2017/02/15/15/17/meal-2069021_960_720.jpg");
        video5.setTitle("Lasagna 6 Ways");
        video5.setVideoId("vjQiE69JVMQ");
        mYoutubeVideo.add(video5);

       // mYoutubeVideo.add(video1);
    //    mYoutubeVideo.add(video2);
     //   mYoutubeVideo.add(video3);
     //   mYoutubeVideo.add(video4);
        return mYoutubeVideo;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.allbars, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_right_menu) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            SendUserToHome();
        } else if (id == R.id.nav_search) {
            SendUserToSearch();

        } else if (id == R.id.nav_map) {
            SendUserToMapsActivity();

        } else if (id == R.id.nav_video) {
            SendUserToVideo();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void SendUserToHome() {
        Intent homeIntent = new Intent(VideoActivity.this, Home.class);
        startActivity(homeIntent);
    }

    private void SendUserToMapsActivity() {
        Intent mapsIntent = new Intent(VideoActivity.this, MapsActivity.class);
        startActivity(mapsIntent);
    }

    private void SendUserToSearch() {
        Intent searchIntent = new Intent(VideoActivity.this, Search.class);
        startActivity(searchIntent);

    }
    private void SendUserToVideo() {
        Intent videoIntent = new Intent(VideoActivity.this, VideoActivity.class);
        startActivity(videoIntent);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }
}
