package com.example.komputer.mycook;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.algolia.instantsearch.core.helpers.Searcher;
import com.algolia.instantsearch.ui.helpers.InstantSearch;
import com.algolia.instantsearch.ui.utils.ItemClickSupport;
import com.algolia.instantsearch.ui.views.Hits;
import com.algolia.instantsearch.ui.views.SearchBox;
import com.algolia.search.saas.Query;
//import com.google.firebase.firestore.CollectionReference;
//import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.jar.Attributes;

public class Search extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Searcher searcher;


    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;


    private static final String ALGOLIA_APP_ID = "YFGWGHB3IZ";
    private static final String ALGOLIA_INDEX_NAME = "Food";
    private static final String ALGOLIA_API_KEY = "a6206f9804ad886e917bd54f101c3d83";
   // private FirebaseFirestore db = FirebaseFirestore.getInstance();
   // private CollectionReference fireRef = db.collection("Food");

    private Hits hits;

    private FilterResultsWindow filterResultsWindow;
    private Drawable arrowDown;
    private Drawable arrowUp;
    private Button buttonFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();
     /*   navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.nav_home:
                        Intent anIntent = new Intent(getApplicationContext(), Home.class);
                        startActivity(anIntent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_search:
                        Intent aIntent = new Intent(getApplicationContext(), Search.class);
                        startActivity(aIntent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_map:
                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                        startActivity(intent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_video:
                        Intent vintent = new Intent(getApplicationContext(), VideoActivity.class);
                        startActivity(vintent);
                        drawer.closeDrawers();
                        break;

                }
                return false;
            }
        });*/



        searcher = Searcher.create(ALGOLIA_APP_ID, ALGOLIA_API_KEY, ALGOLIA_INDEX_NAME);
        InstantSearch helper = new InstantSearch(this, searcher);
        searcher.search(getIntent());
        helper.search();

        hits =  (Hits) findViewById(R.id.hits);
        hits.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView recyclerView, int position, View v) {
                JSONObject hit = hits.get(position);
                String FoodId = null;
                try {
                    FoodId = hit.getString("FoodId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent singleIntent = new Intent (Search.this,Single_Hit_Item.class);
                singleIntent.putExtra("FoodId", FoodId);
                startActivity(singleIntent);
            }
        });


        ((SearchBox) findViewById(R.id.searchBox)).disableFullScreen();
        filterResultsWindow = new FilterResultsWindow.Builder(this, searcher)
                .addCheckBox("Breakfast", "breakfast", true)
                .addCheckBox("Dinner", "dinner", true)
                .addCheckBox("Dessert", "dessert", true)
                .addCheckBox("Supper", "supper", true)
                .build();

        buttonFilter = findViewById(R.id.btn_filter);
        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean willDisplay = !filterResultsWindow.isShowing();
                if (willDisplay) {
                    filterResultsWindow.showAsDropDown(buttonFilter);
                } else {
                    filterResultsWindow.dismiss();
                }
                toggleArrow(buttonFilter, willDisplay);
            }
        });

    }

    @Override
    protected void onNewIntent(@NonNull Intent intent) {
        super.onNewIntent(intent);
        searcher.search(intent);
    }

    @Override
    protected void onStop() {
        filterResultsWindow.dismiss();
        toggleArrow(buttonFilter, false);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        filterResultsWindow.dismiss();
        searcher.destroy();
        toggleArrow(buttonFilter, false);
        super.onDestroy();
    }


    private void toggleArrow(Button b, boolean isUp) {
        final Drawable[] currentDrawables = b.getCompoundDrawables();
        b.setCompoundDrawablesWithIntrinsicBounds(currentDrawables[0], currentDrawables[1], getArrowDrawable(isUp), currentDrawables[3]);
    }

    private Drawable getArrowDrawable(boolean isUp) {
        if (isUp) arrowUp = ContextCompat.getDrawable(this, R.drawable.arrow_up_flat);
        else arrowDown = ContextCompat.getDrawable(this, R.drawable.arrow_down_flat);
        return isUp ? arrowUp : arrowDown;
    }







    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.allbars, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_right_menu) {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            SendUserToHome();
        } else if (id == R.id.nav_search) {
            SendUserToSearch();

        } else if (id == R.id.nav_map) {
            SendUserToMapsActivity();

        } else if (id == R.id.nav_video) {
            SendUserToVideo();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void SendUserToHome() {
        Intent homeIntent = new Intent(Search.this, Home.class);
        startActivity(homeIntent);
    }

    private void SendUserToMapsActivity() {
        Intent mapsIntent = new Intent(Search.this, MapsActivity.class);
        startActivity(mapsIntent);
    }

    private void SendUserToSearch() {
        Intent searchIntent = new Intent(Search.this, Search.class);
        startActivity(searchIntent);

    }
    private void SendUserToVideo() {
        Intent videoIntent = new Intent(Search.this, VideoActivity.class);
        startActivity(videoIntent);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        toggle.syncState();
    }
}
